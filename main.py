import datetime
import os
import subprocess
import threading
import time


from settings import NAME_USB_BUTTON, BUTTON_CODES, PLAYER_MEDIA, PLAYER_IMAGE, CMD_MEDIA, MEDIA_DIR, CMD_IMAGE, \
    BUTTON_MAP, SPLASH_IMAGE, UP_DOWN_BUTTON_TIMER
from utils.common import logger, get_config, check_running_proc, kill_process_by_name, is_rpi, create_landing_screen

_cur_dir = os.path.dirname(os.path.realpath(__file__))


if is_rpi():
    from utils.usb_button import USBButtonReader
    from utils.espeak import trigger_speech
    from utils.braille_display import BrailleDisplay


class BlindKiosk(threading.Thread):
    index = -1
    line_index = 0
    cur_texts = []
    _lock = threading.RLock()
    _last_up_down_btn_time = 0
    _last_left_right_btn_time = 0
    _image_start_time = 0
    playlist = []
    _cur_duration = 5

    def __init__(self):
        super().__init__()
        if is_rpi():
            self._btn_reader = USBButtonReader(name=NAME_USB_BUTTON, codes=BUTTON_CODES, callback=self.on_btn_pressed)
            self._btn_reader.start()
            self.braille = BrailleDisplay()
            if not self.braille.open():
                logger.error('Not Found Braille display!')

    def run(self):
        while True:
            playlist = get_config().get('playlist', [])
            if len(self.playlist) == 0 and len(playlist) > 0:
                logger.debug('Killing the landing page as new playlist is available.')
                kill_process_by_name(PLAYER_IMAGE)

            with self._lock:
                self.playlist = playlist

            if len(self.playlist) == 0:
                self._show_landing_screen()
                time.sleep(.1)
            else:
                if not check_running_proc(PLAYER_MEDIA):
                    if check_running_proc(PLAYER_IMAGE):
                        if self._last_up_down_btn_time > 0:
                            if time.time() - self._last_up_down_btn_time < UP_DOWN_BUTTON_TIMER:
                                continue
                        if time.time() - self._image_start_time > self._cur_duration:
                            logger.debug('>> Killing Image as it is expired.')
                            kill_process_by_name(PLAYER_IMAGE)
                    # Ignore when LEFT/RIGHT button is pressed as it takes some time to start fbi.
                    elif time.time() - self._last_left_right_btn_time > 1:
                        with self._lock:
                            self.index = 0 if (self.index == len(self.playlist) - 1) else (self.index + 1)
                            logger.debug('>> Starting playback from main loop')
                            self.__start_playback(speech=False)
            time.sleep(.1)

    def __start_playback(self, speech=False):
        try:
            cur_conf = self.playlist[self.index]
        except IndexError:
            logger.warning('Playlist is changed during the playback')
            self.index = -1
            return

        if cur_conf.get("status", '').lower() != "enable":
            logger.warning('{} is not enabled, skipping...'.format(cur_conf['file_name']))
            return

        self._last_up_down_btn_time = 0
        today = datetime.date.today().isoformat()
        start_date = cur_conf.get('start_date')
        expire_date = cur_conf.get('expire_date')
        if start_date and today < start_date:
            logger.warning('{} is not started yet...'.format(cur_conf['file_name']))
            return
        if expire_date and today > expire_date:
            logger.warning('{} is expired, skipping...'.format(cur_conf['file_name']))
            return

        file_path = os.path.join(MEDIA_DIR, cur_conf['file_name'])
        self.cur_texts = [line.strip() for line in cur_conf.get('plain_text', '').strip().splitlines() if line.strip()]
        if cur_conf['type'] == 'Video':
            cmd = CMD_MEDIA.format(media_player=PLAYER_MEDIA, file_name=file_path,
                                   audio="hdmi" if get_config()['settings']['audioOutput'] == 'HDMI' else "local")
        else:
            self._cur_duration = int(cur_conf.get('duration', 5))
            self._image_start_time = time.time()
            cmd = CMD_IMAGE.format(image_player=PLAYER_IMAGE, file_name=file_path)
        logger.info('=== Playing `{}`, index: {}'.format(cmd, self.index))
        if is_rpi():
            subprocess.Popen(cmd, shell=True)
        self.line_index = 0
        self.read_line(speech=speech)

    def on_btn_pressed(self, index=0, is_long=False):
        btn_cmd = BUTTON_MAP[index]
        logger.info('Button{} is released, LONG PRESS: {}, cmd: {}'.format(index, is_long, btn_cmd))
        with self._lock:
            if btn_cmd == 'up':
                self._last_up_down_btn_time = time.time()
                self.line_index = max(self.line_index - 1, 0)
                self.read_line()
            elif btn_cmd == 'down':
                self._last_up_down_btn_time = time.time()
                self.line_index = min(self.line_index + 1, len(self.cur_texts) - 1)
                self.read_line()
            elif btn_cmd == 'left':
                self._last_left_right_btn_time = time.time()
                self.index = (self.index - 1) if (self.index > 0) else (len(self.playlist) - 1)
                kill_process_by_name(PLAYER_MEDIA)
                kill_process_by_name(PLAYER_IMAGE)
                self.__start_playback(speech=True)
                logger.debug('Image Player is started? {}'.format(check_running_proc(PLAYER_IMAGE)))
            elif btn_cmd == 'right':
                self._last_left_right_btn_time = time.time()
                self.index = 0 if (self.index == len(self.playlist) - 1) else (self.index + 1)
                kill_process_by_name(PLAYER_MEDIA)
                kill_process_by_name(PLAYER_IMAGE)
                self.__start_playback(speech=True)
                logger.debug('Image Player is started? {}'.format(check_running_proc(PLAYER_IMAGE)))

    def read_line(self, speech=True):
        txt = self.cur_texts[self.line_index]
        logger.info('Reading a line - `{}`'.format(txt))
        self.braille.display_text(txt)
        if speech:
            trigger_speech(txt, audio=get_config()['settings']['audioOutput'])

    @staticmethod
    def _show_landing_screen():
        """
        Display landing screen that indicates how to access interface
        :return:
        """
        if check_running_proc(SPLASH_IMAGE):
            return
        kill_process_by_name(PLAYER_MEDIA)
        kill_process_by_name(PLAYER_IMAGE)
        create_landing_screen()
        cmd = CMD_IMAGE.format(image_player=PLAYER_IMAGE, file_name=SPLASH_IMAGE, duration=3600 * 24 * 30)
        logger.info('=== Displaying landing screen - `{}`'.format(cmd))
        if is_rpi():
            subprocess.Popen(cmd, shell=True)


if __name__ == '__main__':

    time.sleep(3)

    bk = BlindKiosk()
    bk.start()
