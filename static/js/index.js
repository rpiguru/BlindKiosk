tinymce.init({
  selector: '#text_tiny',
  menubar: false,
  inline: false,
  plugins: [
    'lists',
    'autolink'
  ],
  toolbar: [
    'undo redo | bold italic underline | Font-Family fontselect fontsizeselect',
    'forecolor backcolor| alignleft aligncenter alignright alignfull | numlist bullist outdent indent',
      'custom_formats'
  ],
  valid_elements: 'p[style],strong,em,span[style],a[href],ul,ol,li',
  powerpaste_word_import: 'clean',
  powerpaste_html_import: 'clean',
  font_formats: 'Vollkorn=Vollkorn, serif;B612=B612, sans-serif;Alegreya=Alegreya, serif;Muli=Muli, sans-serif;Titillium Web=Titillium Web, sans-serif;Varela=Varela, sans-serif;IBM Plex Serif=IBM Plex Serif, serif;Crimson Text=Crimson Text, serif;Cairo=Cairo, sans-serif;BioRhyme=BioRhyme, serif;Karla=Karla, sans-serif;' +
      'Arial Black=arial black,avant garde;Times New Roman=times new roman,times;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
      fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 48pt 72pt 96pt 128pt 144pt',
  dialog_type : "modal"
});

$(document).on('focusin', function(event) {
  if ($(event.target).closest(".mce-window").length) {
    event.stopImmediatePropagation();
  }
});
$(window).bind("load", function() {
  $('#addModalCenter').on('show.bs.modal', function (e) {
    var modalSubmitBtn = document.getElementById("modalSubmitBtn");
    if(editFlag)
      modalSubmitBtn.innerText = "Update";
    else
    {
      modalSubmitBtn.innerText = "Add";
      document.getElementById("youtubeUrl").value = "";
    }

  });
  $("#progressModal").on('shown.bs.modal',function(){
    $(document).off('focusin.bs.modal');
});
  /**/
  drawData(jsonObj);
});

function drawData(jsonObj)
{
  var jsonArray = jsonObj.playlist;

  for (var i = 0; i < jsonArray.length; i++)
    insertContentRow(i+1, jsonArray[i].id, jsonArray[i].type, jsonArray[i].file_name, jsonArray[i].desc_title, jsonArray[i].text,
        jsonArray[i].start_date, jsonArray[i].expire_date, jsonArray[i].status, jsonArray[i].duration);
}
function insertContentRow(rowIndex, id, paraType, paraFilePath, descTitle, descSummary, paraStartDate, paraExpireDate, paraStatus, paraDuration)
{
  var currentDate = new Date().toISOString().slice(0, 10);
  if( paraExpireDate !== "" && paraExpireDate < currentDate)
    return;
  if ( paraStartDate !== "" && paraStartDate > currentDate)
    return;
  var table = document.getElementById("data_table");
  var row = table.insertRow(rowIndex);
  row.tag = id;
  /* Set status */
  if( paraStatus === "Disable" )
    row.style.backgroundColor = "#d6d8db";
  row.title = paraStatus;
  row.style.backgroundColor = jsonObj.settings.itemBackColor;
  var noCell = row.insertCell(0);
  var typeCell = row.insertCell(1);
  var fileNameCell = row.insertCell(2);
  var descriptionCell = row.insertCell(3);
  var durationCell = row.insertCell(4);
  var liveDateCell = row.insertCell(5);
  var buttonCell = row.insertCell(6);
  /* Draw Reorder Button */
  // var reorderBtn = document.createElement("div");
  // reorderBtn.appendChild(getArrowButton("Up"));
  // reorderBtn.appendChild(getArrowButton("Down"));
  var indexLabel = document.createElement("label");
  indexLabel.innerHTML = table.rows.length-1;
  indexLabel.className = "d-block m-0";
  var upBtn = getArrowButton("Up");
  var downBtn = getArrowButton("Down");
  if( rowIndex === 1)
    upBtn.style.visibility = "hidden";
  else {
    table.rows[rowIndex-1].childNodes[0].childNodes[2].style.visibility = "visible";
    downBtn.style.visibility = "hidden";
  }
  noCell.appendChild(upBtn);
  noCell.appendChild(indexLabel);
  noCell.appendChild(downBtn);

  /* Draw Content Type */
  var typeImg = document.createElement("img");
  if(paraType === "Video")
    typeImg.src = "../static/img/movie.png";
  if(paraType === "Image")
    typeImg.src = "../static/img/image.png";
  if(paraType === "Text")
    typeImg.src = "../static/img/text.png";
  typeImg.tag = paraType;
  typeImg.style.cssText = "width: 4vw;height:4vw;";

  typeCell.appendChild(typeImg);

  /* Draw Duration */
    durationCell.innerText = paraDuration;
  /* Draw Action Buttons*/
  var delBtn = document.createElement("button");
  delBtn.className = "btn btn-danger btn-sm";
  // delBtn.innerText = "Remove";
  delBtn.id = rowIndex;
  delBtn.style.width = "3vw";
  delBtn.style.height = "3vw";
  delBtn.style.marginLeft = "5px";
  var removeIcon = document.createElement("i");
  removeIcon.className = "fas fa-trash-alt";
  delBtn.appendChild(removeIcon);
  delBtn.addEventListener('click', requestRemove);

  var editBtn = document.createElement("button");
  editBtn.className = "btn btn-primary btn-sm";
  editBtn.style.width = "3vw";
  editBtn.style.height = "3vw";
  // editBtn.innerText = "Edit";
  editBtn.id = "edit"+rowIndex;
  editBtn.tag = rowIndex;
  editBtn.addEventListener('click', requestEdit);
  var editIcon = document.createElement("i");
  editIcon.className = "fas fa-pencil-alt";
  editBtn.appendChild(editIcon);

  buttonCell.appendChild(editBtn);
  buttonCell.appendChild(delBtn);

  /* Set Description Content */
  if(paraType === "Text") {
    var descTitleCtl = document.createElement("div");
    if( descTitle == null )
      descTitle = "";
    descTitleCtl.innerHTML = descTitle;
    descTitleCtl.style.fontSize = "2vw";
    let descContentSplitCtl = document.createElement("div");
    descContentSplitCtl.style.height = "5px";
    descContentSplitCtl.style.width = "100%";
    descContentSplitCtl.style.backgroundColor = "#4caf50";
    descriptionCell.appendChild(descTitleCtl);
    descriptionCell.appendChild(descContentSplitCtl);
  }
  var descSummaryCtl = document.createElement("div");
  descSummaryCtl.innerHTML = descSummary;
  descSummaryCtl.innerHTML = descSummaryCtl.innerText;
  descSummaryCtl.title = descSummary;
  descSummaryCtl.style.width = "35vw";
  // descSummaryCtl.style.wordWrap = "break-word";
  descSummaryCtl.className = "text-center";
  descriptionCell.appendChild(descSummaryCtl);

  /* Set File name*/
  var file_path = paraFilePath.split('\\');
  if ( paraType !== "Text")
    fileNameCell.innerHTML = file_path[file_path.length - 1];
  fileNameCell.tag = paraFilePath;

  /* Set Expire Date */
    if(paraStartDate === "" && paraExpireDate === "")
        return;
    var startDateCtl = document.createElement('div');
    startDateCtl.innerText = paraStartDate;
    var splitDiv = document.createElement('div');
    splitDiv.innerText = "|";
    var expireDateCtl = document.createElement('div');
    expireDateCtl.innerText = paraExpireDate;
    liveDateCell.appendChild(startDateCtl);
    liveDateCell.appendChild(splitDiv);
    liveDateCell.appendChild(expireDateCtl);


}
var dataIndex = -1;
var editFlag = false;
var requestRemove = function(event)
{
  dataIndex = event.target.id-1;
  $('#removeModal').modal('show');
};

var updateRow = function (rowIndex, id, paraType, paraFilePath, descTitle, descSummary, paraStartDate, paraExpireDate, paraStatus, paraDuration) {
  var table = document.getElementById("data_table");
  var row = table.rows[rowIndex];
  row.tag = id;
  row.title = paraStatus;

  if( paraStatus === "Enable")
    row.style.backgroundColor = jsonObj.settings.itemBackColor;
  else
    row.style.backgroundColor = "#d6d8db";
  /* Content Type */
  var typeImg = row.cells[1].firstElementChild;
  typeImg.tag = paraType;
  if(paraType === "Video")
    typeImg.src = "../static/img/movie.png";
  if(paraType === "Image")
    typeImg.src = "../static/img/image.png";
  if(paraType === "Text")
    typeImg.src = "../static/img/text.png";
    /* File name */
  var fileNameCell = row.cells[2];
  var file_path = paraFilePath.split('\\');
  if( file_path.length > 0 && paraType !== "Text")
    fileNameCell.innerHTML = file_path[file_path.length - 1];
  fileNameCell.tag = paraFilePath;

    /* Description */
  var descTitleCtl = row.cells[3].firstElementChild;
  descTitleCtl.innerHTML = descTitle;
  var descSummaryCtl = row.cells[3].lastElementChild;
  descSummaryCtl.innerHTML = descSummary;
  descSummaryCtl.innerHTML = descSummaryCtl.innerText;
  descSummaryCtl.title = descSummary;

  /* Duration */
    row.cells[4].innerText = paraDuration;
    /* Expire Date */
  row.cells[5].childNodes[0].innerText = paraStartDate;
  row.cells[5].childNodes[2].innerText = paraExpireDate;

  var currentDate = new Date().toISOString().slice(0, 10);
  if(paraExpireDate < currentDate || paraStartDate > currentDate)
  {
    table.deleteRow(rowIndex);
    ArrangeTableRows();
  }

};
var requestEdit = function(event)
{
  dataIndex = parseInt(event.target.parentNode.parentNode.childNodes[0].innerText)-1;
  var table = document.getElementById("data_table");
  var tbCells = table.rows[dataIndex+1].cells;
  /* Set Data Type */
  var contentType = tbCells[1].firstElementChild.tag;
  document.getElementById("typeSelect").value = contentType;
  typeChange(document.getElementById("typeSelect"));

  /* Set File Name */
  var fileName = tbCells[2].innerHTML;
  var labelFile = document.getElementById("label_file");
  labelFile.tag = fileName;
  if(contentType !== "Text")
    labelFile.value = fileName;

  /* Set the description */
  var descTitle = tbCells[3].firstElementChild.innerText;
  document.getElementById("description_title").value = contentType !== "Text" ? "" : descTitle;
  var descContent = tbCells[3].lastElementChild.title;
  tinymce.get('text_tiny').setContent(descContent);

  /* Set the duration */
    document.getElementById("addDuration").value = tbCells[4].innerText;
  /* Set the Expire Date */
    if ( tbCells[5].childNodes.length > 0 ) {
        var expireDateCtl = document.getElementById("expireDate");
        var startDateCtl = document.getElementById("startDate");
        startDateCtl.value = tbCells[5].childNodes[0].innerText;
        expireDateCtl.value = tbCells[5].childNodes[2].innerText;
    }

  /* Set Status */
  var statusSwitch = document.getElementById("statusSwitch");
  var statusLabel = document.getElementById("statusLabel");
  // statusSwitch.checked = tbCells[4].lastElementChild.tag === "Enabled";
  statusSwitch.checked =  table.rows[dataIndex+1].title === "Enable";
  statusLabel.innerText = table.rows[dataIndex+1].title;
  editFlag = true;
  $('#addModalCenter').modal('show');
};

function removeFunc()
{
  var table = document.getElementById("data_table");
  var dataID = table.rows[dataIndex+1].tag.toString();
  var xhr = new XMLHttpRequest();
  if(dataIndex === -1)
    return;
  xhr.open( "GET", "/remove/"+dataID, false ); // false for synchronous request
  xhr.onload = function(event)
  {
    var response = event.currentTarget.response;
    if (response !== "success") {
      alert(response);
    } else {
      table.deleteRow(dataIndex + 1);
      ArrangeTableRows();
      for (var i = dataIndex + 1; i < table.rows.length; i++) {
        var btn = document.getElementById(i + 1);
        btn.id = i;
      }
      dataIndex = -1;
      $('#removeModal').modal('hide');
    }
  };
  xhr.send( null );
}

function addRowData()
{
  var errMsg="";
  var contentType = document.getElementById("typeSelect").value;
  if(contentType === "undefined" || contentType === "Select Type")
    errMsg = "Please select the Content Type";

  /* Check the description title is valid */
  const descTitle = document.getElementById("description_title").value;
  if(descTitle === "" && contentType === "Text")
    errMsg = "Please write the Content Description Title";

  var descContent = tinymce.get('text_tiny').getContent();
  if(descContent === "")
    errMsg = "Please write the Content description";

  var browseBtn = document.getElementById('files');
  var fileLabel = document.getElementById("label_file");
  var youtubeUtlCtl = document.getElementById("youtubeUrl");
  if(browseBtn.value === "" && contentType !== "Text" && !editFlag)
    errMsg = "Please select the Content File";

  if(fileLabel.value === '' && contentType !== "Text")
    errMsg = "Please select the Content File";

  if(contentType !== "Select Type" && !validateFileType(contentType, fileLabel.value))
    errMsg = "Please select the correct Content File";
  if ( contentType === "Video" && youtubeUtlCtl.value !== "")
    errMsg = "";
  /* Check Expire Data */
  var expireDate = document.getElementById("expireDate").value;
  var startDate = document.getElementById("startDate").value;
  if(startDate !== "" && expireDate !== "" && startDate > expireDate )
      errMsg = "Please Input Correct start and expire date";
  /* Check the status */
  var dataStatus = document.getElementById("statusLabel").innerText;
  if( dataStatus !== "Enable" && dataStatus !== "Disable")
    errMsg = "Please select the data status";
  /* Check the duration */
  var duration = document.getElementById("addDuration").value;
  var durationValue =  parseInt(duration);

  if(contentType === "Video")
      duration = "";
  else if(durationValue < 0 || duration === "")
    errMsg = "Please input the correct duration";
  if(errMsg !== "")
  {
    alert(errMsg);
    return;
  }

  var fileNameLabel = document.getElementById('label_file');

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/upload");
  xhr.contentType = "multipart/form-data";
  xhr.dataType = "application/json";
  var formData = new FormData();
  var progressCtl = document.getElementById("progressBar");
  if(contentType !== "Text" && browseBtn.files.length > 0)
  {
    formData.append("file", browseBtn.files[0]);
    if (xhr.upload && browseBtn.files[0].size >= 5000000) {
      $('#progressModal').modal({
        backdrop: 'static'
      });
      xhr.upload.addEventListener("progress", function (e) {
        var pc = parseInt(e.loaded / e.total * 100);
        progressCtl.innerText = pc + "%";
        progressCtl.style.width = pc.toString()+"%";
        if(pc >99 )
          $('#progressModal').modal('hide');
      }, false);

    }
  }
  if( contentType === "Video" && youtubeUtlCtl.value !== "")
  {
    progressCtl.innerText = "YouTube Uploading";
    progressCtl.style.width = "100%";
    $('#progressModal').modal({
        backdrop: 'static'
      });
  }
  var id;
  if(editFlag)
    id = document.getElementById("data_table").rows[dataIndex+1].tag;
  else
    id = getID(contentType);
  formData.append("id", id);
  formData.append("type", contentType);
  formData.append("desc_title", descTitle);
  formData.append("desc", descContent);
  formData.append("start_date", startDate);
  formData.append("expire_date", expireDate);
  formData.append("status", dataStatus);
  formData.append("duration", duration);
  var file_path = browseBtn.value.split('\\');
  var file_name;
  if( editFlag && browseBtn.value === '')
    file_name = fileNameLabel.innerHTML;
  else
    file_name = file_path[file_path.length-1];
  if(contentType !== "Text") {
    if(contentType === "Video" && youtubeUtlCtl.value !== "" && browseBtn.files.length <= 0)
      formData.append("file_name", youtubeUtlCtl.value);
    else
      formData.append("file_name", file_name);
  }
  formData.append("edit", editFlag);

  xhr.onload = function(event){
    var response = event.currentTarget.response;
    console.log(response);
    $('#progressModal').modal('hide');
    if(response.indexOf("success") !== -1)
    {
      if(dataIndex !== -1)
        updateRow(dataIndex+1, id, contentType, response.substring(8), descTitle, descContent, startDate, expireDate, dataStatus, duration);
      else {
        var table = document.getElementById("data_table");
        insertContentRow(table.rows.length, id, contentType, response.substring(8), descTitle, descContent, startDate, expireDate, dataStatus, duration);
      }
      clearAddDialogContent();
      dataIndex = -1;
    }
    else
      alert(response);
  };
  editFlag = false;
  $('#addModalCenter').modal('hide');
  xhr.send(formData);
}
function readURL(input) {
  if (input.files && input.files[0]) {
    var browseBtn = document.getElementById('files');
    if (browseBtn.value !== "") {
      var theSplit = browseBtn.value.split('\\');
      var labelFile = document.getElementById("label_file");
      labelFile.value = theSplit[theSplit.length - 1];
      // labelFile.innerHTML = theSplit[theSplit.length - 1];
      labelFile.tag = browseBtn.value;
    }
  }
}

function typeChange(select){
  var type = select.value;
  type = type.toLowerCase();
  if( type === "text")
  {
    document.getElementById("fileBtnLabel").style.backgroundColor = "#82b1ff";
    document.getElementById("files").setAttribute("disabled", "disabled");
  }
  else {
    document.getElementById("fileBtnLabel").style.backgroundColor ="";
    document.getElementById("files").disabled = false;
    var filterJson = {
      "image": ".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*",
      "video": ".mp4, .avi, .mpg, .wmv, .mov, .3gp, .flv, video/*"
    };
    document.getElementById("files").accept = filterJson[type];
  }
  var youtubeCtl = document.getElementById("youtubeUrl");
  youtubeCtl.disabled = type !== "video";

  var addDurationCtl = document.getElementById("addDuration");
  addDurationCtl.disabled = type === "video"
  document.getElementById("description_title").disabled = type !== "text"
}

function openModal(){
  clearAddDialogContent();
  editFlag = false;
  dataIndex = -1;
}

function openSettingModal()
{
  var backColor = jsonObj.settings.backgroundColor;
  var splashFlag = jsonObj.settings.splashFlag;
  var audioOutput = jsonObj.settings.audioOutput;
  const itemBackColor = jsonObj.settings.itemBackColor;

  document.getElementById("backColorPicker").value = backColor;
  document.getElementById("contentBackColorPicker").value = itemBackColor;
  document.getElementById("splashToggle").checked = splashFlag === "On" ;
  document.getElementById("audioOutputType").value = audioOutput;
}

function getID(param)
{
  var dateTime = new Date().toLocaleString();
  var id = dateTime + param;
  return id.hashCode();
}
String.prototype.hashCode = function(){
  var hash = 0;
  if (this.length === 0) return hash;
  for (var i = 0; i < this.length; i++) {
    var char = this.charCodeAt(i);
    hash = ((hash<<5)-hash)+char;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
};

function validateFileType(type, filePath)
{
  if( type === "Select Type")
    return false;
  if (type === "Text")
    return true;
  var filterJson = {
    "Image": ".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*",
    "Video": ".mp4, .avi, .mpg, .wmv, .mov, .3gp, .flv, video/*"
  };
  var fileExtension = filePath.split('.');
  if( fileExtension.length > 0)
    fileExtension = fileExtension[fileExtension.length - 1];
  else
    return false;
  return filterJson[type].indexOf(fileExtension.toLowerCase()) !== -1;
}

function clearAddDialogContent()
{
  document.getElementById("typeSelect").selectedIndex = 0;
  var browseBtn = document.getElementById("files");
  browseBtn.value = "";
  var labelFile = document.getElementById("label_file");
  labelFile.value = "";
  var youtubeUrlCtl = document.getElementById("youtubeUrl");
  youtubeUrlCtl.value = "";
  tinymce.get('text_tiny').setContent("");
  var expireDateCtl = document.getElementById("expireDate");
  expireDateCtl.value = "";
  var startDateCtl = document.getElementById("startDate");
  startDateCtl.value = "";
  var addDurationCtl = document.getElementById("addDuration");
  addDurationCtl.value= "5";
  addDurationCtl.disabled = false;

  document.getElementById("statusSwitch").checked = true;
  document.getElementById("statusLabel").innerHTML = "Enable";
  document.getElementById("statusSwitch").checked = true;
  document.getElementById("description_title").value = "";
}

function changeStatus(input)
{
  var statusLabel = document.getElementById("statusLabel");
  statusLabel.innerHTML = input.checked ? "Enable" : "Disable";
}

function updateItemBackColor() {
  const tbBody = document.getElementById("data_table");
  for(let i = 1; i < tbBody.rows.length ;i++)
      tbBody.rows[i].style.backgroundColor = jsonObj.settings.itemBackColor;
}

function saveSettings()
{
  /* Get background color */
  var backColor = document.getElementById("backColorPicker").value;
  /* Get background color */
  var itemBackColor = document.getElementById("contentBackColorPicker").value;
  /* Get item background image */
  const itemBackImgBtn = document.getElementById("itemBackImgInput");
  /* Get Splash Screen Flag */
  var splashFlag = document.getElementById("splashToggle").checked ? "On" : "Off";
  /* Get Audio output type */
  var audioOutputType = document.getElementById("audioOutputType").value;
  var errMsg = "";
  if(audioOutputType === "" || audioOutputType === "Select Audio Output" )
    errMsg = "Please select the Audio Output";
  if(errMsg)
  {
    alert(errMsg);
    return;
  }

  var progressCtl = document.getElementById("progressBar");

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/settings");
  xhr.contentType = "multipart/form-data";
  xhr.dataType = "application/json";
  var formData = new FormData();
  formData.append("backgroundColor", backColor);
  formData.append("itemBackColor", itemBackColor);
  formData.append("splashFlag", splashFlag);
  formData.append("audioOutput", audioOutputType);


  if(itemBackImgBtn.files.length > 0)
  {
    const file_type = itemBackImgBtn.files[0].name.split('.')[1];
    formData.append("file_name", "item_backImg." + file_type);
    formData.append("file", itemBackImgBtn.files[0]);
    if (xhr.upload && itemBackImgBtn.files[0].size >= 5000000) {
      $('#progressModal').modal({
        backdrop: 'static'
      });
      xhr.upload.addEventListener("progress", function (e) {
        var pc = parseInt(e.loaded / e.total * 100);
        progressCtl.innerText = pc + "%";
        progressCtl.style.width = pc.toString()+"%";
        if(pc >99 )
          $('#progressModal').modal('hide');
      }, false);
    }
  }

  xhr.onload = function(event){
    var response = event.currentTarget.response;
    console.log(response);
    $('#progressModal').modal('hide');
    if( response === "Success" )
    {
       jsonObj.settings = {'backgroundColor':backColor, 'itemBackColor':itemBackColor, 'splashFlag':splashFlag, 'audioOutput':audioOutputType};
       updateItemBackColor();
    }
    // alert(response);
  };

  $('#settingModal').modal('hide');
  xhr.send(formData);
}

/* Create Arrow Btton */
function getArrowButton(direction)
{
  var button = document.createElement("button");
  var icon = document.createElement("i");
  if( direction === "Up")
    icon.className = "d-block fas fa-chevron-up text-success";
  else
    icon.className = "d-block fas fa-chevron-down text-success";
  button.appendChild(icon);
  button.className = "btn p-0";
  // button.tag = direction;
  button.addEventListener('click', function(event){
    reorderRow(event, direction);
}, false);
  return button;
}
/* Reorder Row */
var reorderRow = function(event, actionType){
  var cell = event.target.parentNode.parentNode;
  var rowIndex = parseInt(cell.innerText);
  var tb = document.getElementById("data_table");
  if (tb.rows.length <= 2)
    return;
  var row = tb.rows[rowIndex];
  var refRow = null;
  if (actionType === "Up") {
    refRow = tb.rows[rowIndex - 1];
    row.childNodes[0].childNodes[1].innerHTML = rowIndex - 1;
    refRow.childNodes[0].childNodes[1].innerHTML = rowIndex;
    row.parentNode.insertBefore(row.parentNode.removeChild(row), refRow);

    row.childNodes[0].childNodes[0].focus();
  }
  else
  {
    refRow = tb.rows[rowIndex + 1];
    row.childNodes[0].childNodes[1].innerHTML = rowIndex + 1;
    refRow.childNodes[0].childNodes[1].innerHTML = rowIndex;
    row.parentNode.insertBefore(row.parentNode.removeChild(refRow), row);

    row.childNodes[0].childNodes[2].focus();
  }
  if( rowIndex === 1 )
  {
    /* up button */
    row.childNodes[0].childNodes[0].style.visibility = "visible";
    refRow.childNodes[0].childNodes[0].style.visibility = "hidden";
  }
  if( rowIndex === tb.rows.length - 1 )
  {
    /* down button */
    row.childNodes[0].childNodes[2].style.visibility = "visible";
    refRow.childNodes[0].childNodes[2].style.visibility = "hidden";
  }
  if( rowIndex === 2 && actionType === "Up")
  {
    /* up button */
    row.childNodes[0].childNodes[0].style.visibility = "hidden";
    refRow.childNodes[0].childNodes[0].style.visibility = "visible";
  }
  if( rowIndex === tb.rows.length-2 && actionType === "Down" )
  {
    /* down button */
    row.childNodes[0].childNodes[2].style.visibility = "hidden";
    refRow.childNodes[0].childNodes[2].style.visibility = "visible";
  }
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/reorder");
  xhr.contentType = "multipart/form-data";
  xhr.dataType = "application/json";
  var formData = new FormData();
  // formData.append("reorder_direction", event.target.tag);
  formData.append("first_content_id", row.tag);
  formData.append("second_content_id", refRow.tag);

  xhr.onload = function(event){
    var response = event.currentTarget.response;
    console.log(response);
  };
  xhr.send(formData);
};

function ArrangeTableRows() {
  var dataTable = document.getElementById('data_table');
  for (var i=1; i< dataTable.rows.length; i++)
    dataTable.rows[i].childNodes[0].childNodes[1].innerText = i;
}

function focusDate(event)
{
  document.getElementById('startDate').focus();
}
