$(function() {
    $("form[name='login']").validate({
        rules: {
            email: {
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: "Please enter a User Name",

            password: {
                required: "Please enter password"
            }

        }});
});
