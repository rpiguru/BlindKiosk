import gc
import glob
import json
import netifaces
import os
import logging.config
import platform
import shutil
import signal
import subprocess
import sys
import time
import html2text
import imgkit
from pytube import YouTube
from PIL import Image, ImageDraw, ImageFont

_cur_dir = os.path.dirname(os.path.realpath(__file__))
_par_dir = os.path.join(_cur_dir, os.pardir)

if _par_dir not in sys.path:
    sys.path.append(_par_dir)


from settings import CONFIG_FILE, MEDIA_DIR, FONT_PATH, SPLASH_IMAGE


def is_rpi():
    try:
        return 'arm' in os.uname()[4]
    except AttributeError:
        return False


if is_rpi():
    os.environ['DISPLAY'] = "0.0"


logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('BK')


if not os.path.exists(CONFIG_FILE):
    logger.warning('=== Config file({}) is missing, restoring the default one'.format(CONFIG_FILE))
    shutil.copy(os.path.join(_cur_dir, os.pardir, 'config.json'), CONFIG_FILE)


def update_config_file(data):
    json_data = get_config()
    json_data.update(data)
    with open(CONFIG_FILE, 'w') as outfile:
        json.dump(json_data, outfile, indent=2)


def update_row(row):
    playlist = get_config().get('playlist', [])
    for i, p in enumerate(playlist[:]):
        if p['id'] == row['id']:
            if p['file_name']:
                if not row['file_name'] == '':
                    remove_media(p['file_name'])
                else:
                    row['file_name'] = p['file_name']
            playlist[i] = row
            update_config_file({"playlist": playlist})
            return row['file_name']


def remove_config_data(data_id):
    json_data = json.loads(open(CONFIG_FILE).read())
    removed_file = ''
    index = 0
    for obj in json_data['playlist']:
        if obj['id'] == data_id:
            removed_file = obj['file_name']
            json_data["playlist"].pop(index)
            break
        index += 1

    with open(CONFIG_FILE, 'w') as outfile:
        json.dump(json_data, outfile, indent=2)
    return removed_file


def get_config():
    return json.loads(open(CONFIG_FILE).read())


def remove_media(file_name):
    file_path = os.path.join(MEDIA_DIR, file_name)
    if os.path.exists(file_path):
        os.remove(file_path)


def update_settings_config(data):
    try:
        json_data = get_config()
        json_data.update(data)
        with open(CONFIG_FILE, 'w') as outfile:
            json.dump(json_data, outfile, indent=2)
            return "Success"
    except Exception as e:
        logger.error('Web: Failed to Save Settings - {}'.format(e))
        return '\tException: {}'.format(e)


def reorder_content(first_content_id, second_content_id):
    playlist = get_config().get('playlist', [])
    first_content_index = second_content_index = -1
    for i, p in enumerate(playlist[:]):
        if p['id'] == first_content_id:
            first_content_index = i
        if p['id'] == second_content_id:
            second_content_index = i
        if first_content_index != -1 and second_content_index != -1:
            break
    row = playlist[first_content_index]
    playlist[first_content_index] = playlist[second_content_index]
    playlist[second_content_index] = row
    update_config_file({"playlist": playlist})
    return "success"


def download_by_url(url):
    try:
        yt = YouTube(url)
        yt.streams.filter(file_extension='mp4').first().download(MEDIA_DIR)
        result = yt.title + '.mp4'
        return result
    except Exception as e:
        result = '\tException: {}'.format(e)
        return result


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi():
        cpuserial = "0000000000000000"
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26].lstrip('0')
        f.close()
        return cpuserial
    else:
        return '12345678'


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_free_gpu_size():
    gc.collect()
    if is_rpi():
        try:
            pipe = os.popen('sudo vcdbg reloc stats | grep "free memory"')
            data = pipe.read().strip()
            pipe.close()
            return data
        except OSError:
            logger.error('!!! Failed to get free GPU size! ')
            tot, used, free = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])
            logger.error('Total: {}, Used: {}, Free: {}'.format(tot, used, free))
    return 0


def disable_screen_saver():
    if is_rpi():
        os.system('sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')


def get_screen_resolution():
    """
    Get resolution of the screen
    :return:
    """
    if is_rpi():
        pipe = os.popen('fbset -s')
        data = pipe.read().strip()
        pipe.close()
        for line in data.splitlines():
            if line.startswith('mode'):
                w, h = [int(p) for p in line.split('"')[1].split('x')]
                return w, h
    else:
        return 1920, 1080


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    if is_rpi():
        if len(os.popen("ps -aef | grep -i '%s' "
                        "| grep -v 'grep' | awk '{ print $3 }'" % proc_name).read().strip().splitlines()) > 0:
            return True
        return False
    else:
        return True


def kill_process_by_name(proc_name):
    """
    Kill process by its name
    :param proc_name:
    :return:
    """
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line:
            pid = int(line.split(None, 1)[0])
            logger.debug('??? Found PID({}) of `{}`, killing...'.format(pid, proc_name))
            os.kill(pid, signal.SIGKILL)


def convert_html2text(html):
    p = html2text.HTML2Text()
    p.emphasis_mark = ""
    p.strong_mark = ""
    return p.handle(html)


def convert_html2image(html, file_path):
    options = {"xvfb": ""} if is_rpi() else {}
    if imgkit.from_string(html, file_path, options=options):
        img = Image.open(file_path)
        w, h = get_screen_resolution()
        # Resize if the result image is too big. (but keep ratio)
        if img.size[0] > w:
            img = img.resize((w, int(img.size[1] * w / img.size[0])), Image.ANTIALIAS)
        if img.size[1] > h:
            img = img.resize((int(img.size[0] * h / img.size[1]), h), Image.ANTIALIAS)
        img.save(file_path)


def get_ethernet_ip_address():
    intf = "eth0"
    for f in glob.glob('/sys/class/net/*'):
        name = f.split('/')[-1]
        if name not in {"lo", "wlan0"}:
            intf = name
            break
    return get_ip_address(intf)


def get_ip_address(ifname='wlan0'):
    """
    Get assigned IP address of given interface
    :param ifname: interface name such as wlan0, eth0, etc
    :return: If not on RPi, returns the IP address of LAN
    """
    if platform.system() == 'Windows':
        return '192.168.1.170'
    if not is_rpi():
        ifname = 'enp3s0'
    try:
        return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except Exception as e:
        print('Failed to get IP address of {}, reason: {}'.format(ifname, e))


def create_landing_screen():
    s_time = time.time()
    while True:
        eth = get_ethernet_ip_address()
        wlan = get_ip_address('wlan0')
        if eth is not None or wlan is not None:
            break
        elif time.time() - s_time > 10:
            logger.error('Failed to get network address of LAN/WiFi')
        time.sleep(1)
    img = Image.open(os.path.join(_cur_dir, os.path.pardir, 'static', 'img', 'bk_logo.jpg'))
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype(FONT_PATH, size=img.size[0] // 45)
    if eth is not None or wlan is not None:
        txt = "For configuration, please visit following link(s):"
        w, h = font.getsize(txt)
        draw.text(xy=((img.size[0] - w) / 2, img.size[1] - img.size[1] // 5), fill=(0, 0, 0), font=font, text=txt)
        txt = "{} {}".format(
            "LAN - http://{}".format(eth) if eth else '',
            "{}  WiFi - http://{}".format(" or" if eth else "", wlan) if wlan else '')
        w, h = font.getsize(txt)
        draw.text(xy=((img.size[0] - w) / 2, img.size[1] - img.size[1] // 7), fill=(0, 0, 0), font=font, text=txt)
        img.save(SPLASH_IMAGE, dpi=(300, 300))
    else:
        txt = "No network found, please connect LAN or WiFi and reboot."
        w, h = font.getsize(txt)
        draw.text(xy=((img.size[0] - w) / 2, img.size[1] - img.size[1] // 5), fill=(0, 0, 0), font=font, text=txt)
        img.save(SPLASH_IMAGE, dpi=(300, 300))


if __name__ == '__main__':

    create_landing_screen()
