import os
import subprocess
import threading


from utils.common import is_rpi, logger, kill_process_by_name


convert_dict = {
    " oz": " ounces",
    " lb": ' pound',
    " lbs": " pounds",
    " pcs": " pieces",
    " kg": " kilograms",
    " g": " grams",
    " in. hg": " inches of mercury",
    "-": "minus "
}


def trigger_speech(text, audio='HDMI'):
    threading.Thread(target=_trigger_speech, args=(text, audio, )).start()


PREFIX = "A"


def _trigger_speech(text, audio="HDMI"):
    """
        Trigger the current string to speech/braille display
    :param audio:
    :param text:
    :type text: str
    :return:
    """
    os.system("amixer cset numid=3 {}".format(2 if audio == 'HDMI' else 1))
    for k, v in convert_dict.items():
        text = text.lower().replace(k, v)
    if is_rpi() and text.strip():
        kill_process_by_name('espeak')
        logger.info('*** Speaking on {} - `{}`'.format(audio, text))
        subprocess.Popen("espeak -v english-us -g 1 -p 35 -s 210 \"{} {}\"".format(PREFIX, text),
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
