#!/usr/bin/env bash

echo "Installing BlindKiosk Service..."
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt update

sudo apt install -y build-essential python3-dev python3-smbus libpython3-dev python3-pip gcc nginx wkhtmltopdf xvfb
sudo apt install -y cmake build-essential python3-dev python3-pip fbi
sudo apt install -y python3-pil
sudo apt install -y fonts-freefont-ttf

sudo pip3 install -U pip
sudo pip3 install -r requirements.txt

# Install custom splash video
sudo bash install_custom_splash_screen.sh

sudo apt install -y espeak omxplayer

# Install watchdog
sudo bash ${cur_dir}/script/install_watchdog.sh

# Install pngview to overlay custom texts
sudo cp ${cur_dir}/script/pngview/libraspidmx.so* /usr/lib
sudo cp ${cur_dir}/script/pngview/pngview /usr/local/bin/
sudo chmod +x /usr/local/bin/pngview

# HDMI Configuration
echo "hdmi_force_hotplug=1" | sudo tee -a /boot/config.txt
echo "config_hdmi_boost=4" | sudo tee -a /boot/config.txt
echo "hdmi_drive=2" | sudo tee -a /boot/config.txt

# Increase GPU memory size
echo "gpu_mem=384" | sudo tee -a /boot/config.txt

echo "Disabling the booting logo..."
echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt

# Disable some services to reduce booting time
sudo systemctl disable hciuart
sudo mkdir /etc/systemd/system/networking.service.d
sudo touch /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "[Service]" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "TimeoutStartSec=1" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
sudo rm /etc/systemd/system/dhcpcd.service.d/wait.conf

# Disable the blinking cursor
sudo sed -i -- "s/^exit 0/TERM=linux setterm -foreground black >\/dev\/tty0\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/TERM=linux setterm -clear all >\/dev\/tty0\\nexit 0/g" /etc/rc.local

sudo rm /etc/nginx/sites-available/default
sudo rm /etc/nginx/sites-enabled/default

sudo cp bk.nginx /etc/nginx/sites-available/bk
sudo ln -s /etc/nginx/sites-available/bk /etc/nginx/sites-enabled/bk
sudo service nginx restart

sudo apt install screen
sudo sed -i -- "s/^exit 0/amixer cset numid=3 2\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -mS bk -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S bk -X stuff \"cd \/home\/pi\/BlindKiosk\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S bk -X stuff \"gunicorn wsgi -b 127.0.0.1:8000 --daemon -w2 --log-file=bk.log\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S bk -X stuff \"python3 main.py\\\\r\"\\nexit 0/g" /etc/rc.local
