# BlindKiosk

Blindness-Accessible Information Kiosk

## Installation

- Clone this repository
    
    ```shell script
    cd ~
    git clone https://gitlab.com/rpiguru/BlindKiosk
    ```

- Install backend service
    
    ```shell script
    cd ~/BlindKiosk
    bash setup.sh
    ```
