import os

SERVER_PORT = 80

CONFIG_FILE = os.path.expanduser('~/.bk_config.json')

NAME_USB_BUTTON = "DragonRise Inc.   Generic   USB  Joystick  "

MEDIA_DIR = os.path.expanduser('~/media')
if not os.path.exists(MEDIA_DIR):
    os.makedirs(MEDIA_DIR)

PLAYER_MEDIA = 'omxplayer'
PLAYER_IMAGE = 'fbi'

CMD_MEDIA = '{media_player} -o {audio} {file_name}'
CMD_IMAGE = '{image_player} -d /dev/fb0 -T 1 -1 --noverbose --fitwidth --autozoom {file_name}'
HTML_TEMPLATE = "<div style=\"background-color:{color}; min-width: {width}px; min-height: {height}px; " \
                "padding: 100px\">{content}</div>"

"""
Button 1: 288 (['BTN_JOYSTICK', 'BTN_TRIGGER']), up
Button 2: 289 (BTN_THUMB), up 
Button 3: 290 (BTN_THUMB2), up
Button 4: 291 (BTN_TOP), up
Button 5: 292 (BTN_TOP2), up
Button 6: 293 (BTN_PINKIE), up
"""
BUTTON_CODES = [288, 289, 290, 291, 292, 293]
BUTTON_MAP = ['up', 'down', 'left', 'right', None, None]

# If someone presses the UP/DOWN buttons to read the braille, this timer will pause the content item advancement
# until the timer expires or the LEFT/RIGHT buttons are pressed.
UP_DOWN_BUTTON_TIMER = 30


FONT_PATH = '/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf'
SPLASH_IMAGE = '/tmp/bk_splash.jpg'

try:
    from local_settings import *
except ImportError:
    pass
