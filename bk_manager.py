#!/usr/bin/env python3

import json
import os
import time
from flask import Flask, flash, render_template, redirect, session, request

from settings import MEDIA_DIR, SERVER_PORT, HTML_TEMPLATE
from utils.common import get_config, remove_config_data, update_config_file, remove_media, \
    convert_html2text, logger, update_row, download_by_url, update_settings_config, reorder_content, \
    convert_html2image, get_screen_resolution

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'bkiosk'
app.config['SECRET_KEY'] = 'super secret key'


@app.route('/login')
def init():
    return render_template(
        'login.html'
    )


@app.route('/login', methods=['POST'])
def login():
    user_name = request.form["email"]
    user_password = request.form["password"]
    user_info = get_config()["user_info"]
    if user_name == user_info["user_name"]:
        if user_password == user_info["user_password"]:
            session['logged_in'] = True
            return redirect('/')
        else:
            response = "User password is incorrect"
    else:
        response = "User name is incorrect"
    flash(response)
    return render_template(
        'login.html',
    )


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return main()


@app.route('/')
def main():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        config = get_config()
        playlist = config.get('playlist', [])
        resp = []
        current_time = time.strftime('%Y-%m-%d')
        for i, jsonObj in enumerate(playlist[:]):
            if jsonObj.get('expire_date') != "" and jsonObj.get('expire_date') < current_time:
                continue
            if jsonObj.get('start_date') != "" and current_time < jsonObj.get('start_date'):
                continue
            resp.append(jsonObj)
        resp = json.dumps({'playlist': resp, 'settings': config.get('settings')})
        return render_template(
            'index.html',
            data=resp
        )


@app.route('/remove/<data_id>', methods=['GET'])
def remove(data_id):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        try:
            file_name = remove_config_data(data_id)
            resp = "success"
            if not file_name == "":
                remove_media(file_name)

        except Exception as e:
            resp = '\tException: {}'.format(e)
        return resp


@app.route('/upload', methods=['POST'])
def update_config():
    try:
        edit_flag = request.form["edit"]
        file_name = None
        if request.form["type"] == "Text":
            file_name = '{}.jpg'.format(time.time().__int__())
            convert_html2image(
                html=HTML_TEMPLATE.format(
                    color=get_config()["settings"]['backgroundColor'],
                    content="<div>{}</div>".format(request.form["desc"]),
                    width=get_screen_resolution()[0],
                    height=get_screen_resolution()[1]
                ),
                file_path=os.path.join(MEDIA_DIR, file_name))
        else:
            if request.files:
                _file = request.files['file']
                file_name = request.form["file_name"]
                _file.save(os.path.join(MEDIA_DIR, file_name))
            else:
                youtube_url = request.form["file_name"]
                if youtube_url != '':
                    result = download_by_url(youtube_url)
                    if "Exception" in result:
                        return result
                    file_name = result
        plain_txt = convert_html2text(request.form["desc"])
        response = 'success'
        if file_name is None:
            file_name = request.form.get('file_name', '')
        json_obj = dict(id=request.form["id"], file_name=file_name, type=request.form['type'],
                        desc_title=request.form['desc_title'], text=request.form['desc'], plain_text=plain_txt,
                        start_date=request.form['start_date'], duration=request.form['duration'],
                        expire_date=request.form['expire_date'], status=request.form['status'])

        if edit_flag == 'false':
            playlist = get_config().get('playlist', [])
            playlist.append(json_obj)
            update_config_file({"playlist": playlist})
            response += '_' + file_name
        else:
            response += '_'+update_row(json_obj)
    except Exception as e:
        logger.error('Web: Failed to upload data - {}'.format(e))
        response = '\tException: {}'.format(e)
    return response


@app.route('/settings', methods=['POST'])
def update_settings():
    try:
        item_back_img_name = ""
        if request.files:
            _file = request.files['file']
            item_back_img_name = request.form["file_name"]
            file_path = os.path.join(MEDIA_DIR, item_back_img_name)
            if os.path.exists(file_path):
                os.remove(file_path)
            _file.save(file_path)
        setting_obj = dict(backgroundColor=request.form["backgroundColor"],
                           itemBackColor=request.form["itemBackColor"], itemBackImg=item_back_img_name,
                           splashFlag=request.form["splashFlag"], audioOutput=request.form['audioOutput'])
        return update_settings_config({"settings": setting_obj})
    except Exception as e:
        logger.error('Web: Failed to Save Settings - {}'.format(e))
        response = '\tException: {}'.format(e)
    return response


@app.route('/reorder', methods=['POST'])
def reorder():
    try:
        first_content_id = request.form['first_content_id']
        second_content_id = request.form['second_content_id']
        reorder_content(first_content_id, second_content_id)
        response = "Success"
    except Exception as e:
        logger.error('Web: Failed to Save Settings - {}'.format(e))
        response = '\tException: {}'.format(e)
    return response


if __name__ == '__main__':

    app.run(host='0.0.0.0', port=SERVER_PORT, debug=True)
